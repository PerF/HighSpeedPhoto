/*
 * TimeoutHolder.hpp
 *
 *  Created on: 28 mars 2016
 *      Author: dhenry
 */

#ifndef SRC_TIMEOUTHOLDER_HPP_
#define SRC_TIMEOUTHOLDER_HPP_

class TimeoutHolder {
public:
	TimeoutHolder();
	void start(unsigned int delay);
	bool isExpired();
private:
	unsigned long timeoutTimeStamp; // ms
};

#endif /* SRC_TIMEOUTHOLDER_HPP_ */
