/*
 * SensorTestState.hpp
 *
 *  Created on: 28 mars 2016
 *      Author: dhenry
 */

#ifndef SRC_SENSORTESTSTATE_HPP_
#define SRC_SENSORTESTSTATE_HPP_

#include "Hal.hpp"
#include "IState.hpp"
#include "TimeoutHolder.hpp"
#include "Configuration.hpp"

class SensorTestState: public IState {
public:
	SensorTestState(Hal& hardware, const Configuration& configuration, TimeoutHolder& timeout);
	void onStart();
	void onExit();

private:
	Hal& hardware;
	const Configuration& configuration;
	TimeoutHolder& timeout;
};

#endif /* SRC_SENSORTESTSTATE_HPP_ */
