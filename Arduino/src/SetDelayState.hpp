#ifndef __STATES_H
#define __STATES_H

#include "IState.hpp"
#include "Hal.hpp"
#include "Configuration.hpp"

class SetDelayState: public IState {
public:
	SetDelayState(Hal& hal, Configuration& config);
	void onStart();
	void loop();
	void onExit();

private:
	Hal& hardware;
	Configuration& config;
	unsigned int delay;
};

#endif
