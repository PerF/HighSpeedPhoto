/*
 * EndOfCycle.hpp
 *
 *  Created on: 12 janv. 2016
 *      Author: dhenry
 */

#include "IState.hpp"
#include "Hal.hpp"

class EndOfCycle: public IState {
public:
	EndOfCycle(Hal& hardware);
	void onStart();
private:
	Hal& hardware;
};

