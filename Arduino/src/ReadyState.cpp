/*
 * ReadyState.cpp
 *
 *  Created on: 11 janv. 2016
 *      Author: dhenry
 */

#include "ReadyState.hpp"
#include <LiquidCrystal.h>
#include "Hal.hpp"


ReadyState::ReadyState(Hal& hardware) :
		hardware(hardware) {
}

void ReadyState::onStart() {
	LiquidCrystal lcd = this->hardware.getLCD();
	lcd.clear();
	lcd.print("Ready to shoot");
}

