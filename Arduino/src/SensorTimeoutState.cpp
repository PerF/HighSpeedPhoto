/*
 * SensorTimeoutState.cpp
 *
 *  Created on: 12 janv. 2016
 *      Author: dhenry
 */

#include "SensorTimeoutState.hpp"
#include <LiquidCrystal.h>
#include "Hal.hpp"

SensorTimeoutState::SensorTimeoutState(Hal& hardware) :
		hardware(hardware) {
}

void SensorTimeoutState::onStart() {
	LiquidCrystal lcd = this->hardware.getLCD();
	lcd.clear();
	lcd.print("Sensor time out!");
}
