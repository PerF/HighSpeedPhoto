#include "Hal.hpp"
#include "Arduino.h"
#include <LiquidCrystal.h>

#include "Pinout.hpp"

#define BUTTON_BOUNCE_TIME	50
#define SENSOR_BOUNCE_TIME	5

bool Hal::isTurnDetected = false;
bool Hal::up = false;
int volatile Hal::rotDirection = 0;

Hal::Hal(LiquidCrystal& lcd) :
		lcd(lcd) {
	lcd.begin(16, 2);
	initBounce(rotButton, ROT_SW, BUTTON_BOUNCE_TIME);
	initBounce(startButton, START_BUTTON_INPUT, BUTTON_BOUNCE_TIME);
	initBounce(sensorInput, SENSOR_INPUT, SENSOR_BOUNCE_TIME);

	pinMode(TEST_BUTTON_INPUT, INPUT_PULLUP);
	pinMode(FLASH_OUTPUT, OUTPUT);
	pinMode(CAMERA_OUTPUT, OUTPUT);

	pinMode(ROT_SW, INPUT_PULLUP);
	pinMode(ROT_DT, INPUT_PULLUP);
	pinMode(ROT_CLK, INPUT_PULLUP);

	digitalWrite(FLASH_OUTPUT, LOW);
}

void Hal::initBounce(Bounce& bounce, uint8_t pin, unsigned int bounceDelay) {
	pinMode(pin, INPUT_PULLUP);
	bounce.attach(pin);
	bounce.interval(bounceDelay);
}

LiquidCrystal& Hal::getLCD() {
	return this->lcd;
}

void Hal::setLcdLed(unsigned short brightness) {
	analogWrite(LCD_LED, map(brightness, 0, 100, 0, 255));
}
void Hal::lcdFadeOut() {
	for (int i = 100; i >= 0; i--) {
		this->setLcdLed(i);
		delay(5);
	}
}
void Hal::triggerFlash() {
	digitalWrite(FLASH_OUTPUT, HIGH);
	delay(5);
	digitalWrite(FLASH_OUTPUT, LOW);
}

void Hal::rotInterruptCallback() {
	static unsigned long lastInterruptTime = 0;
	unsigned long interruptTime = millis();

	// If interrupts come faster than XX ms, assume it's a bounce and ignore
	if (interruptTime - lastInterruptTime > 50) {
		rotDirection = digitalRead(ROT_DT) ? +1 : -1;
	}
	lastInterruptTime = interruptTime;
}

int Hal::getRotDirection() {
	return Hal::rotDirection;
}

void Hal::resetRotDirection() {
	Hal::rotDirection = 0;
}

bool Hal::rotButtonTriggered() {
	return inputTriggered(rotButton);
}

bool Hal::startButtonTriggered() {
	return inputTriggered(startButton);
}

bool Hal::startButtonMaintained() {
	return !digitalRead(START_BUTTON_INPUT);
}

bool Hal::sensorInputTriggered() {
	return inputTriggered(sensorInput);
}

bool Hal::isTestModeActive() {
	return LOW == digitalRead(TEST_BUTTON_INPUT);
}

bool Hal::inputTriggered(Bounce& input) {
	input.update();
	return input.fell();
}

void Hal::openCamera() {
	digitalWrite(CAMERA_OUTPUT, HIGH);
	delay(100);
}

void Hal::closeCamera() {
	digitalWrite(CAMERA_OUTPUT, LOW);
	delay(100);
}
