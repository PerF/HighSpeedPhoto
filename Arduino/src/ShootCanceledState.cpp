/*
 * ShootCanceledState.cpp
 *
 *  Created on: 25 mars 2016
 *      Author: dhenry
 */

#include "ShootCanceledState.hpp"

ShootCanceledState::ShootCanceledState(Hal& hardware) :
		hardware(hardware) {

}

void ShootCanceledState::onStart() {
	LiquidCrystal lcd = this->hardware.getLCD();
	lcd.clear();
	lcd.print("Shoot canceled");
}

