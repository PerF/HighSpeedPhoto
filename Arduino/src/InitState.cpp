/*
 * InitState.cpp
 *
 *  Created on: 11 janv. 2016
 *      Author: dhenry
 */

#include "Arduino.h"
#include "InitState.hpp"

InitState::InitState(Hal& hardware) :
		hardware(hardware) {
}

void InitState::onStart() {
	Serial.print("init state -> on start\n");
	LiquidCrystal& lcd = hardware.getLCD();
	lcd.noCursor();
	lcd.clear();
	lcd.print("Hello!");
	hardware.setLcdLed(100);
	delay(1000);
	lcd.clear();
	lcd.print("Ready...");
}

void InitState::loop() {
}

void InitState::onExit() {
	Serial.print("init state -> on exit\n");
	hardware.getLCD().clear();
	hardware.getLCD().print("Flash!");
}

