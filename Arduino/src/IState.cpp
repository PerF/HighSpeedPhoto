#include "IState.hpp"

IState::~IState()
{}

void IState::setTransitions(Transition* transitions, int numberOfTransitions) {
  this->transitions = transitions;
  this->numberOfTransitions = numberOfTransitions;
}

Transition* IState::getTransitions() {
  return this->transitions;
}

int IState::getNumberOfTransitions() {
  return this->numberOfTransitions;
}

void IState::onStart() {}

void IState::loop() {}

void IState::onExit() {}
