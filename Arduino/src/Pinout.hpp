#ifndef __PINOUT_H
#define __PINOUT_H

#include "Arduino.h"

#define LCD_RS      3
#define LCD_ENABLE  4
#define LCD_D4      5
#define LCD_D5      6
#define LCD_D6      7
#define LCD_D7      8
#define LCD_LED     9 // PWM

#define ROT_SW      12
#define ROT_DT      11
#define ROT_CLK     10

#define TEST_BUTTON_INPUT	A0
#define START_BUTTON_INPUT  A1
#define CAMERA_OUTPUT		A2
#define SENSOR_INPUT        A3
#define FLASH_OUTPUT        A4

#endif
