#include "Arduino.h"
#include "Transition.hpp"
#include "IState.hpp"


Transition::Transition(Transition::predicate_t predicate, IState* nextState) :
  predicate(predicate), nextState(nextState)
{
  if (!nextState || !predicate) {
    Serial.print("Null pointer for transition!!!\n");
  }
}

bool Transition::isActive() {
  return this->predicate();
}

IState* Transition::getNextState() {
  return this->nextState;
}

