#ifndef __TRANSITION_H
#define __TRANSITION_H

#include "IState.hpp"

class Transition {

  public:
    typedef bool (*predicate_t)(void);
    
    Transition(predicate_t predicate, IState* nextState);
    
    bool isActive();
    IState* getNextState();

  private:
    predicate_t predicate;
    IState* nextState;
};


#endif
