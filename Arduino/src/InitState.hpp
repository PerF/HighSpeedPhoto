/*
 * InitState.hpp
 *
 *  Created on: 11 janv. 2016
 *      Author: dhenry
 */

#ifndef SRC_INITSTATE_HPP_
#define SRC_INITSTATE_HPP_

#include "IState.hpp"
#include "Hal.hpp"

class InitState: public IState {
public:
	InitState(Hal& hal);
	void onStart();
	void loop();
	void onExit();

private:
	Hal& hardware;
};

#endif /* SRC_INITSTATE_HPP_ */
