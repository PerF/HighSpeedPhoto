#ifndef __ISTATE_H
#define __ISTATE_H

class Transition;

class IState {

  public:
	virtual ~IState();
    virtual void onStart();
    virtual void loop();
    virtual void onExit();

    virtual void setTransitions(Transition* transitions, int numberOfTransitions);
    virtual Transition* getTransitions();
    virtual int getNumberOfTransitions(); 

  private:
    Transition* transitions;
    int numberOfTransitions;
};

#endif

