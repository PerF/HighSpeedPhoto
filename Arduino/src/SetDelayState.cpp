#include "SetDelayState.hpp"

#include <EnableInterrupt.h>
#include <LiquidCrystal.h>

#include "Arduino.h"
#include "Pinout.hpp"

SetDelayState::SetDelayState(Hal& hardware, Configuration& config) :
		hardware(hardware), config(config) {
	this->delay = this->config.getDelay();
}

void SetDelayState::onStart() {
	Serial.print("Set Delay State: on start\n");

	LiquidCrystal& lcd = this->hardware.getLCD();
	lcd.clear();
	lcd.print("Delay = ");
	lcd.print(this->delay, DEC);

	Serial.print("Set Delay State: started\n");

	enableInterrupt(ROT_CLK, Hal::rotInterruptCallback, FALLING);
}

void SetDelayState::loop() {
	int rotDirection = this->hardware.getRotDirection();
	if (rotDirection) {        // do this only if rotation was detected
		bool isPreciseAdjustment = this->hardware.startButtonMaintained();
		delay += rotDirection * (isPreciseAdjustment ? 1 : 10);

		if (!isPreciseAdjustment) {
			delay = (delay / 10) * 10;
		}

		if (delay > 10000) { // prevent overflow and delay > 10s at the same time
			delay = 0;
		}
		this->hardware.resetRotDirection();          // do NOT repeat IF loop until new rotation detected

		LiquidCrystal lcd = this->hardware.getLCD();
		lcd.clear();
		lcd.print("Delay = ");
		lcd.print(delay, DEC);
	}
}

void SetDelayState::onExit() {
	Serial.print("Set Delay State: exit\n");
	this->config.setDelay(this->delay);
	disableInterrupt (ROT_CLK);
}

