#include "Configuration.hpp"

Configuration::Configuration(unsigned int delay) :
		delay(delay) {
}

void Configuration::setDelay(unsigned int delay) {
	this->delay = delay;
}

unsigned int Configuration::getDelay() const {
	return this->delay;
}

unsigned int Configuration::getSensorTimeout() const {
	return 30000; // TODO: hardcoded
}
