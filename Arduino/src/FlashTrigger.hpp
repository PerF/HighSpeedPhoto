/*
 * FlashTrigger.hpp
 *
 *  Created on: 12 janv. 2016
 *      Author: dhenry
 */

#include "IState.hpp"
#include "Hal.hpp"
#include "Configuration.hpp"

class FlashTrigger: public IState {
public:
	FlashTrigger(Hal& hardware, Configuration& config);

	void onStart();

private:
	Hal& hardware;
	Configuration& config;

};

