#include <LiquidCrystal.h>

#include "Pinout.hpp"
#include "Hal.hpp"
#include "Configuration.hpp"

#include "Transition.hpp"
#include "InitState.hpp"
#include "ReadyState.hpp"
#include "WaitForSensor.hpp"
#include "SensorTestState.hpp"
#include "SetDelayState.hpp"
#include "FlashTrigger.hpp"
#include "EndOfCycle.hpp"
#include "SensorTimeoutState.hpp"
#include "ShootCanceledState.hpp"
#include "TimeoutHolder.hpp"

#define SET_TRANSITIONS(state, transitions) state.setTransitions(transitions, sizeof(transitions) / sizeof(*transitions) );

LiquidCrystal lcd(LCD_RS, LCD_ENABLE, LCD_D4, LCD_D5, LCD_D6, LCD_D7);
Hal hal(lcd);
Configuration config(300);

bool sensorTriggered();
bool rotSwitchTriggered();
bool startButtonTriggered();
bool startInTestMode();
bool alwaysTrue();
bool sensorInTimeout();
void evaluateStateMachine();

TimeoutHolder timeout;

InitState initState(hal);
SetDelayState setDelayState(hal, config);
ReadyState readyState(hal);
WaitForSensor waitForSensor(hal, config, timeout);
SensorTestState sensorTestState(hal, config, timeout);
FlashTrigger flashTrigger(hal, config);
EndOfCycle endOfCycle(hal);
SensorTimeoutState sensorTimeoutState(hal);
ShootCanceledState shootCanceledState(hal);

Transition initStateTransitions[] = { Transition(alwaysTrue, &readyState) };
Transition readyStateTransitions[] = { Transition(startButtonTriggered, &waitForSensor), Transition(rotSwitchTriggered,
		&setDelayState), Transition(startInTestMode, &sensorTestState) };
Transition setDelayStateTransitions[] = { Transition(rotSwitchTriggered, &readyState) };
Transition waitForSensorTransitions[] = { Transition(sensorTriggered, &flashTrigger), Transition(sensorInTimeout,
		&sensorTimeoutState), Transition(startButtonTriggered, &shootCanceledState) };
Transition sensorTestTransitions[] = { Transition(sensorTriggered, &endOfCycle), Transition(sensorInTimeout,
		&sensorTimeoutState), Transition(startInTestMode, &shootCanceledState) };
Transition flashTriggerTransitions[] = { Transition(alwaysTrue, &endOfCycle) };
Transition sensorTimeoutStateTransitions[] = { Transition(alwaysTrue, &endOfCycle) };
Transition shootCanceledTransitions[] = { Transition(alwaysTrue, &endOfCycle) };
Transition endOfCycleTransitions[] = { Transition(alwaysTrue, &readyState) };

IState* currentState = &initState;

void setup() {
	Serial.begin(9600);
	SET_TRANSITIONS(initState, initStateTransitions);
	SET_TRANSITIONS(setDelayState, setDelayStateTransitions);
	SET_TRANSITIONS(readyState, readyStateTransitions);
	SET_TRANSITIONS(waitForSensor, waitForSensorTransitions);
	SET_TRANSITIONS(sensorTestState, sensorTestTransitions);
	SET_TRANSITIONS(flashTrigger, flashTriggerTransitions);
	SET_TRANSITIONS(endOfCycle, endOfCycleTransitions);
	SET_TRANSITIONS(shootCanceledState, shootCanceledTransitions);
	SET_TRANSITIONS(sensorTimeoutState, sensorTimeoutStateTransitions);

	currentState->onStart();
}

// the loop function runs over and over again forever
void loop() {
	evaluateStateMachine();
}

void evaluateStateMachine() {
	currentState->loop();
	Transition* transitions = currentState->getTransitions();
	for (int i = 0; i < currentState->getNumberOfTransitions(); i++) {
		if (transitions[i].isActive()) {
			Serial.print("Transition active, next state\n");
			currentState->onExit();
			currentState = transitions[i].getNextState();
			currentState->onStart();
			break;
		}
	}
}

bool sensorInTimeout() {
	return timeout.isExpired();
}

bool sensorTriggered() {
	return hal.sensorInputTriggered();
}

bool rotSwitchTriggered() {
	return hal.rotButtonTriggered();
}

bool startButtonTriggered() {
	return hal.startButtonTriggered() && !hal.isTestModeActive();
}

bool startInTestMode() {
	return hal.startButtonTriggered() && hal.isTestModeActive();
}

bool alwaysTrue() {
	return true;
}
