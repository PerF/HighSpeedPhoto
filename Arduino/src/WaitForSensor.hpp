/*
 * WaitForSensor.hpp
 *
 *  Created on: 11 janv. 2016
 *      Author: dhenry
 */

#ifndef SRC_WAITFORSENSOR_HPP_
#define SRC_WAITFORSENSOR_HPP_

#include "IState.hpp"
#include "Hal.hpp"
#include "TimeoutHolder.hpp"
#include "Configuration.hpp"

class WaitForSensor: public IState {
public:
	WaitForSensor(Hal& hardware, Configuration& config, TimeoutHolder& timeout);
	void onStart();

private:
	Hal& hardware;
	Configuration& config;
	TimeoutHolder& timeout;
};

#endif /* SRC_WAITFORSENSOR_HPP_ */
