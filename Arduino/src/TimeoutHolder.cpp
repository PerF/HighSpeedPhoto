/*
 * TimeoutHolder.cpp
 *
 *  Created on: 28 mars 2016
 *      Author: dhenry
 */

#include "TimeoutHolder.hpp"
#include "Arduino.h"

TimeoutHolder::TimeoutHolder() :
		timeoutTimeStamp(0) {
}

void TimeoutHolder::start(unsigned int delay) {
	this->timeoutTimeStamp = millis() + delay;
}

bool TimeoutHolder::isExpired() {
	return millis() > this->timeoutTimeStamp;
}
