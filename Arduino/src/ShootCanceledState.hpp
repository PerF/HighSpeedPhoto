/*
 * ShootCanceledState.hpp
 *
 *  Created on: 25 mars 2016
 *      Author: dhenry
 */

#ifndef SRC_SHOOTCANCELEDSTATE_HPP_
#define SRC_SHOOTCANCELEDSTATE_HPP_

#include "IState.hpp"
#include "Hal.hpp"

class ShootCanceledState: public IState {
public:
	ShootCanceledState(Hal& hardware);

	void onStart();

private:
	Hal& hardware;
};

#endif /* SRC_SHOOTCANCELEDSTATE_H_ */
