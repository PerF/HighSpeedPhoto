/*
 * FlashTrigger.cpp
 *
 *  Created on: 12 janv. 2016
 *      Author: dhenry
 */

#include "Arduino.h"
#include <LiquidCrystal.h>
#include "FlashTrigger.hpp"

FlashTrigger::FlashTrigger(Hal& hardware, Configuration& config) : hardware(hardware), config(config)
{
}

void FlashTrigger::onStart() {
	delay(this->config.getDelay());
	this->hardware.triggerFlash();

	LiquidCrystal lcd = this->hardware.getLCD();
	lcd.clear();
	lcd.print("Success!");
}
