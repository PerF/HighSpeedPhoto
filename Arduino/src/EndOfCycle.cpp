/*
 * EndOfCycle.cpp
 *
 *  Created on: 12 janv. 2016
 *      Author: dhenry
 */

#include "Arduino.h"
#include "EndOfCycle.hpp"

EndOfCycle::EndOfCycle(Hal& hardware):hardware(hardware) {
}

void EndOfCycle::onStart() {
	delay(100);
	this->hardware.closeCamera();
	this->hardware.setLcdLed(100);

	delay(1000);
}
