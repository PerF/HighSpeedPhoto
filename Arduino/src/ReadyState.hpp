/*
 * ReadyState.hpp
 *
 *  Created on: 11 janv. 2016
 *      Author: dhenry
 */

#ifndef SRC_READYSTATE_HPP_
#define SRC_READYSTATE_HPP_

#include "IState.hpp"
#include "Hal.hpp"

class ReadyState: public IState {
public:
	ReadyState(Hal& hardware);
	void onStart();

private:
	Hal& hardware;
};
#endif /* SRC_READYSTATE_HPP_ */
