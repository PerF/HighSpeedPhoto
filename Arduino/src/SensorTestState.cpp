/*
 * SensorTestState.cpp
 *
 *  Created on: 28 mars 2016
 *      Author: dhenry
 */

#include "Arduino.h"
#include "SensorTestState.hpp"

SensorTestState::SensorTestState(Hal& hardware, const Configuration& configuration, TimeoutHolder& timeout) :
		hardware(hardware), configuration(configuration), timeout(timeout) {
}

void SensorTestState::onStart() {
	LiquidCrystal lcd = this->hardware.getLCD();
	lcd.clear();
	lcd.print(" ==== Test ==== ");
	lcd.setCursor(0, 1);
	lcd.print("Wait for sensor!");
	this->hardware.lcdFadeOut();

	this->timeout.start(this->configuration.getSensorTimeout());
}

void SensorTestState::onExit() {
	delay(this->configuration.getDelay());
	LiquidCrystal lcd = this->hardware.getLCD();
	lcd.clear();
	lcd.print(" ==== Test ==== ");
	lcd.setCursor(4, 1);
	lcd.print("Success!");

	for (int i = 0; i < 5; i++) {
		this->hardware.setLcdLed(100);
		delay(50);
		this->hardware.setLcdLed(0);
		delay(100);
	}
}
