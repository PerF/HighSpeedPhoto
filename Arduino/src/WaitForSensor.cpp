/*
 * WaitForSensor.cpp
 *
 *  Created on: 11 janv. 2016
 *      Author: dhenry
 */

#include "Arduino.h"
#include "WaitForSensor.hpp"
#include "TimeoutHolder.hpp"
#include "Hal.hpp"
#include <LiquidCrystal.h>

WaitForSensor::WaitForSensor(Hal& hardware, Configuration& config, TimeoutHolder& timeout) :
		hardware(hardware), config(config), timeout(timeout) {
}

void WaitForSensor::onStart() {
	LiquidCrystal lcd = this->hardware.getLCD();
	lcd.clear();
	lcd.print("Wait for sensor!");
	this->hardware.lcdFadeOut();
	this->hardware.openCamera();

	this->timeout.start(this->config.getSensorTimeout());
}

