#ifndef __CONFIGURATION_H
#define __CONFIGURATION_H

class Configuration {
public:
	Configuration(unsigned int delay);
	void setDelay(unsigned int delay);

	unsigned int getDelay() const;
	unsigned int getSensorTimeout() const;

private:
	unsigned int delay;

};

#endif
