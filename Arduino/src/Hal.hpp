#ifndef __HAL_H
#define __HAL_H

#include <LiquidCrystal.h>
#include <Bounce2.h>

class Hal {
public:
	Hal(LiquidCrystal& lcd);

	LiquidCrystal& getLCD();
	void setLcdLed(unsigned short brightness); // percent
	void lcdFadeOut();
	void triggerFlash();

	static void rotInterruptCallback();
	static int getRotDirection();
	static void resetRotDirection();

	bool rotButtonTriggered();
	bool startButtonTriggered();
	bool startButtonMaintained();
	bool sensorInputTriggered();
	bool isTestModeActive();

	void openCamera();
	void closeCamera();

private:
	LiquidCrystal& lcd;

	// Rotary encoder status
	static bool isTurnDetected;
	static bool up;
	static volatile int rotDirection;

	Bounce rotButton;
	Bounce startButton;
	Bounce sensorInput;

	bool inputTriggered(Bounce& input);

	static void initBounce(Bounce& bounce, uint8_t pin, unsigned int bounceDelay);

};

#endif
