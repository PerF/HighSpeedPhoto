/*
 * SensorTimeoutState.hpp
 *
 *  Created on: 12 janv. 2016
 *      Author: dhenry
 */

#ifndef SRC_SENSORTIMEOUTSTATE_HPP_
#define SRC_SENSORTIMEOUTSTATE_HPP_

#include "IState.hpp"
#include "Hal.hpp"

class SensorTimeoutState: public IState {
public:
	SensorTimeoutState(Hal& hardware);
	void onStart();

private:
	Hal& hardware;
};

#endif /* SRC_SENSORTIMEOUTSTATE_HPP_ */
