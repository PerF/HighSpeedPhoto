package dh.raspberry;

import java.util.concurrent.TimeUnit;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

import dh.raspberry.rt.RealTimeExecutor;

public class Dummy implements ISample {

	private RealTimeExecutor executor;
	private final GpioController gpio;
	private GpioPinDigitalOutput led;

	public Dummy() {
		this.gpio = GpioFactory.getInstance();
	}

	@Override
	public void run() {
		System.out.println("Real action");

		this.executor.enqueue(() -> {
			System.out.println("high");
			this.led.high();
		}, 1, TimeUnit.SECONDS);
		this.executor.enqueue(() -> {
			System.out.println("low");
			this.led.low();
		}, 2, TimeUnit.SECONDS);

		System.out.println("Stopping");
		this.executor.stop(10, TimeUnit.SECONDS);
		led.clearProperties();
	}

	@Override
	public void init() {
		this.led = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_29, "LED", PinState.LOW);
		this.executor = new RealTimeExecutor();
	}

}
