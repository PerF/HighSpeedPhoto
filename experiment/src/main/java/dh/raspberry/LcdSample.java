package dh.raspberry;

import com.pi4j.wiringpi.Gpio;
import com.pi4j.wiringpi.Lcd;

public class LcdSample implements ISample {

	private int handle;

	@Override
	public void run() {
		Lcd.lcdClear(handle);
		Lcd.lcdDisplay(handle, 1);
		Lcd.lcdHome(handle);
		Lcd.lcdPosition(handle, 0, 0);
		Lcd.lcdPuts(handle, "Coin Coin!");
		Lcd.lcdPosition(handle, 1, 1);
		Lcd.lcdPuts(handle, "o< o< o< o< o<");

		wait(10000);
		Lcd.lcdClear(handle);
		Lcd.lcdDisplay(handle, 0);
	}

	private void wait(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void init() {
		Gpio.wiringPiSetupGpio();
		this.handle = Lcd.lcdInit(2, 16, 4, 21, 20, 26, 19, 13, 6, 0, 0, 0, 0);
	}

}
