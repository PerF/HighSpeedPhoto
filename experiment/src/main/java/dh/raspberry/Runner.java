package dh.raspberry;

public class Runner {

	public void exec(ISample sample) {
		sample.init();
		sample.run();
	}

	public static void main(String[] args) {
		new Runner().exec(new LcdSample());
	}

}
