package dh.raspberry.rt;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class RealTimeExecutor {

	private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1, new RealTimeThreadFactory());

	public ScheduledFuture<?> enqueue(Runnable r, long delay, TimeUnit unit) {
		return executor.schedule(r, delay, unit);
	}

	public <T> ScheduledFuture<T> enqueue(Callable<T> c, long delay, TimeUnit unit) {
		return executor.schedule(c, delay, unit);
	}

	public void stop(long timeout, TimeUnit unit) {
		try {
			this.executor.shutdown();
			this.executor.awaitTermination(timeout, unit);
			if (!executor.isShutdown()) {
				this.executor.shutdownNow();
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
}
