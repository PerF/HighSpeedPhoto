package dh.raspberry;

import java.util.concurrent.TimeUnit;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import dh.raspberry.rt.RealTimeExecutor;

public class Barrier implements ISample {

	private final class OnBarrier implements GpioPinListenerDigital {
		@Override
		public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
			if (event.getState() == PinState.HIGH) {
				System.out.println("intrusion");
				sensor.removeAllListeners();
				laser.low();
				executor.enqueue(() -> {
					laser.high();
				}, 1, TimeUnit.SECONDS);
				executor.enqueue(() -> {
					sensor.addListener(new OnBarrier());
					System.out.println("Ready");
				}, 2, TimeUnit.SECONDS);
			}
		}
	}

	private RealTimeExecutor executor;

	private GpioController gpio;
	private GpioPinDigitalInput sensor;
	private GpioPinDigitalOutput laser;

	@Override
	public void run() {
		try {
			laser.high();
			Thread.sleep(100);
			sensor.addListener(new OnBarrier());
			Thread.sleep(20000);
		} catch (Exception ex) {
			System.out.println("Exception");
		} finally {
			System.out.println("Over");
			gpio.shutdown();
			System.out.println("out");
			System.exit(0);
		}

	}

	@Override
	public void init() {
		executor = new RealTimeExecutor();
		gpio = GpioFactory.getInstance();
		sensor = gpio.provisionDigitalInputPin(RaspiPin.GPIO_25, "Barrier", PinPullResistance.PULL_UP);
		laser = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_28, PinState.LOW);
		laser.setShutdownOptions(true, PinState.LOW, PinPullResistance.PULL_UP);
	}

}
