package dh.raspberry;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.io.gpio.event.PinEventType;

public class Rotary implements ISample {

	private GpioController gpio;
	private GpioPinDigitalInput dtPin;
	private GpioPinDigitalInput valuePin;

	@Override
	public void init() {
		gpio = GpioFactory.getInstance();
		dtPin = gpio.provisionDigitalInputPin(RaspiPin.GPIO_00, "DT", PinPullResistance.PULL_UP);
		dtPin.setDebounce(100, PinState.HIGH);
		valuePin = gpio.provisionDigitalInputPin(RaspiPin.GPIO_01, "CLK", PinPullResistance.PULL_UP);
		dtPin.addListener(new GpioPinListenerDigital() {

			@Override
			public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
				if (event.getEventType() == PinEventType.DIGITAL_STATE_CHANGE && event.getState() == PinState.HIGH) {
					System.out.println("tick " + valuePin.getState());

				}
			}
		});
	}

	@Override
	public void run() {
		try {
			Thread.sleep(10000);
		} catch (Exception ex) {
			System.out.println("Shutdown");
		} finally {
			gpio.removeAllListeners();
			gpio.removeAllTriggers();
			gpio.shutdown();
		}

	}

}
